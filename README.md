# Mukund's YASnippets for Java

A set of personalized yas snippets for java development.

## List of snippet

| snippet | description         |
|---------|---------------------|
| cdoc    | JavaDoc for classes |
| mdoc    | JavaDoc for methods |
|---------|---------------------|

